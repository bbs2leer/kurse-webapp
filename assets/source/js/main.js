/**
 * JavaScript für die Ausführung von Funktionalitäten auf der Clientseite.
 */
jQuery(document).ready(() => {

    /**
     * Betätigen des Drucken-Buttons
     */
    jQuery('.printBtn').click((e) => {
        e.preventDefault();
        window.print();
    });

    /**
     * Modal zum Löschen der Daten aufrufen
     */
    jQuery('.clearAll a').click((e) => {
        e.preventDefault();
        jQuery('#clearInformationModal').modal('show');
    });

    /**
     * Modal zum Versenden der Daten
     */
    jQuery('.mailBtn').click((e) => {
        e.preventDefault();
        jQuery('#mailModal').modal('show');
    });


    /**
     * Cookie-Hinweis einblenden
     */
    if (!readCookie("cookieBanner")){
        jQuery("body").append('<div class="cookieBanner"><div class="cookieBanner__inner container"><p>Diese Seite verwendet Cookies. Durch die weitere Nutzung der Webseite stimmen Sie der Verwendung von Cookies zu. Weitere Informationen erhalten Sie in der <a href="index.php?action=privacy">Datenschutzerklärung</a>.</p><span class="cookieBanner__btn btn btn-primary btn-sm">Alles klar!</span></div></div>');
    }

    /**
     * Cookie-Hinweis nach Buttonklick ausblenden
     */
    jQuery('.cookieBanner__btn').on('click', () => {
        console.log("clicked");
        createCookie("cookieBanner", true, 30);
        jQuery('.cookieBanner').hide();
    })
});


/**
 * Erzeugen von Cookies
 * @param name
 * @param value
 * @param days
 */
function createCookie(name, value, days) {
    let expires = "";
    if (days) {
        const date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        expires = "; expires=" + date.toGMTString();
    }
    document.cookie = name + "=" + value + expires + "; path=/";
}


/**
 * Lesen von Cookies
 * @param name
 * @returns {*}
 */
function readCookie(name) {
    const cookieName = name + "=";
    const cookieContent = document.cookie.split(';');
    for (let i = 0; i < cookieContent.length; i++) {
        var c = cookieContent[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(cookieName) == 0) return c.substring(cookieName.length, c.length);
    }
    return null;
}