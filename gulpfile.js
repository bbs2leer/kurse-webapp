/**
 * Gulp Tasks
 *
 * Install devDependencies by package.json or cli:
 *
 * npm install gulp
 * npm install gulp-sourcemaps
 * npm install gulp-ruby-sass
 *
 *
 */


/**
 * import devDependencies
 */
var gulp = require('gulp'),
    sourcemaps = require('gulp-sourcemaps'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    uglify = require('gulp-uglify'),
    pump = require('pump'),
    babel = require('gulp-babel');


/**
 * ################## TASKS ##################
 *
 *
 * Define default task
 */
gulp.task('default', ['watchFiles']);


/**
 * Define file watch task
 */
gulp.task('watchFiles', ['build-sass', 'build-javascript'], function() {
    gulp.watch('assets/source/**/*.scss',['build-sass']);
    gulp.watch('assets/source/**/*.js',['minifiy-javascript']);
});


/**
 * Define sass compile task
 * Using autoprefixer for css prefixes
 */
gulp.task('build-sass', function () {
    return sass('./assets/source/css/main.scss', { sourcemap: true, style: "compressed" })
        .on('error', sass.logError)
        .pipe(autoprefixer({
            browsers: ['last 5 versions', "ie 8", "ie 9", "> 1%"],
            cascade: false
        }))
        .pipe(sourcemaps.write('.', {
            includeContent: true,
            sourceRoot: 'source'
        }))
        .pipe(gulp.dest('./assets/css'));
});



/**
 * Define javascript minify and compiler task
 * Compile ES6, ES2015 to ES5
 */
gulp.task('build-javascript', function (callback) {
    pump([
            gulp.src('./assets/source/js/*.js'),
            sourcemaps.init(),
            babel({
                presets: ['env']
            }),
            uglify(),
            sourcemaps.write('.', {
                includeContent: true,
                sourceRoot: 'source'
            }),
            gulp.dest('./assets/js')
        ],
        callback
    );
});

