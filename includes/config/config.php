<?php

class Configuration
{

    /**
     * Pfad zur Anwendung
     * Beachten: Absoluter Pfad schließt ohne Slash ab
     *
     * Bei Anpassung:
     * - Pfad an der RewriteBase in der .htaccess vornehmen  (relative Pfade, beginnend und abschließend mit einem Slash)
     * - Pfade (start_url, scope) in manifest.json anpassen (relative Pfade)
     *
     * @var string
     */
    public static $path = "http://localhost/kurswahl2";


    /**
     * Adresse für eingehende Mails
     * @var string
     */
    public static $mailAddress = "info@bbs2leer.de";


    /**
     * Betreff fur eingehende Mails
     * @var string
     */
    public static $mailSubject = "Kurswahl (Berufliches Gymnasium Technik)";

}