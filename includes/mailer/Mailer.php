<?php

/**
 * Class Mailer
 */
class Mailer
{

    /**
     * Adresse
     * @var
     */
    private $address;


    /**
     * Betreff
     * @var
     */
    private $subject;


    /**
     * Nachricht
     * @var
     */
    private $message;


    /**
     * Status, ob bereits gesendet
     * @var bool
     */
    private $posted = false;


    /**
     * Adresse setzen
     * @param $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }


    /**
     * Betreff setzen
     * @param $subject
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
    }


    /**
     * Nachricht setzen
     * @param $message
     */
    public function setMessage($message){
        $this->message = $message;
    }


    /**
     * Header zurückgeben
     * @return string
     */
    private function getHeader()
    {
        $header[] = 'MIME-Version: 1.0';
        $header[] = 'Content-type: text/html; charset=utf-8';
        $header[] = 'Content-Transfer-Encoding: quoted-printable';
        $header[] = 'From: Kurswahl <noreply@kurswahl.bbs2leer.de>';
        return implode("\r\n", $header);
    }


    /**
     * Status zurückgeben
     * @return bool
     */
    public function getStatus(){
        return $this->posted;
    }


    /**
     * Gibt zurück, ob das Captcha richtig eingegeben wurde
     * @return bool
     */
    public function checkCaptcha(){
        $inputCaptcha = Request::post("captcha");
        return ($inputCaptcha == Storage::getCaptcha());
    }


    /**
     * Mail versenden, Status zurückgeben
     * @return bool
     */
    public function send(){
        if (mail($this->address, $this->subject, $this->message, $this->getHeader())){
            $this->posted = true;
        }
        return $this->posted;
    }


}
