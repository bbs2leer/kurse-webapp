<?php

//Direkter Urlaub erlauben
define('APP', true);

//Storage-Klasse laden
require_once("../storage/Storage.php");


/**
 * Class Captcha
 */
class Captcha
{


    /**
     * Erzeugt ein Bild und gibt es aus
     */
    public static function output()
    {

        $value = self::randomValue();
        $valNumber = 10;
        $width = imagefontwidth($valNumber) * strlen($value);
        $height = imagefontheight($valNumber);

        header("Content-type: image/png");
        $image = imagecreate($width, $height);

        $background = imagecolorallocate($image, 255, 255, 255);
        $color = imagecolorallocate($image, 0, 0, 0);

        imagestring($image, $valNumber, 0, 0, $value, $color);
        imagepng($image);
    }


    /**
     * Erzeugt zufällige Zahlen, stellt eine Rechnung auf gibt diese als String zurück
     * @return string
     */
    private static function randomValue()
    {
        $n1 = rand(10, 25);
        $n2 = rand(1, 19);
        $n3 = rand(10, 25);
        $sum = $n1 + $n2 - $n3;
        self::saveAnswer($sum);

        return $n1 . " + " . $n2 . " - " . $n3;
    }


    /**
     * Speichert das Ergebnis
     * @param $answer
     */
    private static function saveAnswer($answer)
    {
        Storage::setCaptcha($answer);
    }

}


/**
 * Aufruf der Methode
 */
Captcha::output();

