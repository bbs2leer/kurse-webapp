<?php
if (!defined('APP')) {
    exit();
}

//Starte Session
session_start();

class Storage
{


    /**
     * Vorname zurückgeben
     * @return string
     */
    public static function getFirstname()
    {
        if (isset($_SESSION['firstname'])) {
            return $_SESSION['firstname'];
        } else {
            return "";
        }
    }


    /**
     * Nachname zurückgeben
     * @return string
     */
    public static function getLastname()
    {
        if (isset($_SESSION['lastname'])) {
            return $_SESSION['lastname'];
        } else {
            return "";
        }
    }

    /**
     * Zweite Fremdsprache zurückgeben
     * @return string
     */
    public static function getParticipatedForeignLanguage()
    {
        if (isset($_SESSION['foreignLanguage'])) {
            return $_SESSION['foreignLanguage'];
        } else {
            return "";
        }
    }

    /**
     * Dritte Fremdsprache zurückgeben
     * @return string
     */
    public static function getSecondParticipatedForeignLanguage()
    {
        if (isset($_SESSION['secondForeignLanguage'])) {
            return $_SESSION['secondForeignLanguage'];
        } else {
            return "";
        }
    }


    /**
     * Einzelne Prüfungsfächer als Kürzel zurückgeben (z.B. ma)
     * @param $number
     * @return string
     */
    public static function getCourse($number)
    {
        if (!empty($_SESSION['course'][$number])) {
            return $_SESSION['course'][$number];
        } else {
            return null;
        }
    }

    /**
     * Einzelne Prüfungsfächer mit voller Bezeichnung zurückgeben (z.B. Mathematik)
     * @param $number
     * @return string
     */
    public static function getFullCourse($number)
    {
        if (!empty(self::getCourse($number))) {
            return Courses::getCourse(self::getCourse($number));
        } else {
            return "nicht gewählt";
        }
    }


    /**
     * Git gewählten Schwerpunkt zurück
     * @return mixed
     */
    public static function getMainCourse()
    {
        return $_SESSION['mainCourse'];
    }


    /**
     * Gibt gewählten Schwerpunkt als vollen Namen zurück
     * @return string
     */
    public static function getFullMainCourse()
    {
        if (!empty($_SESSION['mainCourse'])) {
            $mainCourses = Courses::getMainCourses();
            return $mainCourses[$_SESSION['mainCourse']];
        } else {
            return "nicht gewählt";
        }
    }


    /**
     * Gibt den Wert des Captchas zurück
     * @return mixed
     */
    public static function getCaptcha()
    {
        return $_SESSION['captcha'];
    }


    /**
     * Komplette Session und damit die Daten zerstören
     */
    public static function destroy()
    {
        session_destroy();
    }


    /**
     * Speichert den Vornamen
     * @param $firstname
     */
    public static function setFirstname($firstname)
    {
        $_SESSION['firstname'] = $firstname;
    }


    /**
     * Speichert den Nachnamen
     * @param $lastname
     */
    public static function setLastname($lastname)
    {
        $_SESSION['lastname'] = $lastname;
    }


    /**
     * Speichert die erste Fremdsprachenauswahl
     * @param $language
     */
    public static function setParticipatedForeignLanguage($language)
    {
        $_SESSION['foreignLanguage'] = $language;
    }


    /**
     * Speichert die zweite Fremdsprachenauswahl
     * @param $language
     */
    public static function setSecondParticipatedForeignLanguage($language)
    {
        $_SESSION['secondForeignLanguage'] = $language;
    }


    /**
     * Speichert einen Kurs
     * @param $number
     * @param $subject
     */
    public static function setCourse($number, $subject)
    {
        $_SESSION['course'][$number] = $subject;
    }


    /**
     * Löscht einen Kurs
     * @param $number
     */
    public static function deleteCourse($number)
    {
        unset($_SESSION['course'][$number]);
    }


    /**
     * Speichert den gewählten Schwerpunkt
     * @param $mainCourse
     */
    public static function setMainCourse($mainCourse)
    {
        $_SESSION['mainCourse'] = $mainCourse;
    }


    /**
     * Speichert den Wert des Captchas
     * @param $answer
     */
    public static function setCaptcha($answer)
    {
        $_SESSION['captcha'] = $answer;
    }

}
