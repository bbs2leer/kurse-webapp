<?php

class Courses
{

    /**
     * Definiert die Schwerpunkte
     * @return array
     */
    public static function getMainCourses()
    {
        return array(
            "mt" => "Gestaltungs- und Medientechnik",
            "me" => "Mechatronik",
            "it" => "Informationstechnik",
        );
    }

    /**
     * Verbindet Schwerpunkte mit Kurse
     * @param $mainCourse
     * @return mixed
     */
    public static function getMappedMainCourses($mainCourse){
        if (!empty($mainCourse)){
            $mapping = array(
                "mt" => "d",
                "me" => "m",
                "it" => "if",
            );
            return $mapping[$mainCourse];
        }
       return null;
    }


    /**
     * Gibt alle verfügbare Kurse
     * @return array
     */
    public static function getCourses()
    {
        return array(
            "d" => "Drucktechnik",
            "de" => "Deutsch",
            "if" => "Informatik",
            "m" => "Mechatronik",
            "ge" => "Geschichte",
            "iv" => "Informationsverarbeitung",
            "ph" => "Physik",
            "fm" => "Fachpraxis Mechatronik",
            "w" => "Betriebs- und Volkswirtschaft",
            "ma" => "Mathematik",
            "sp" => "Sport",
            "en" => "Englisch",
            "sn" => "Spanisch",
            "nl" => "Niederländisch",
            "re" => "Religion",
            "wn" => "Werte und Normen",
            "fd" => "Fachpraxis Drucktechnik",
        );
    }

    /**
     * Gibt angebotene Fremdsprachkurse zurück
     * @return array
     */
    public static function getForeignLanguageCourses()
    {
        $foreignLangCourses = array("sn", "nl"); // Verfügbare Fremdsprachkurse
        $courses = self::getCourses();
        $tempArr = array();
        foreach ($foreignLangCourses as &$lang) {
            $tempArr[$lang] = $courses[$lang];
        }
        return $tempArr;
    }

    /**
     * Gibt Kurse mit voller Bezeichnung zurück
     * @param $arr
     * @return array
     */
    public static function getMappedCourses($arr){
        $courses = self::getCourses();
        $tempArr = array();
        foreach ($arr as &$c) {
            $tempArr[$c] = $courses[$c];
        }
        return $tempArr;
    }

    /**
     * Gibt einen Kurse mittels des Kürzels zurück
     * @param $course
     * @return mixed
     */
    public static function getCourse($course)
    {
        $courses = self::getCourses();
        return $courses[$course];
    }

}
