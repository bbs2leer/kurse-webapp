<?php

/**
 * Class Redirect
 */
class Redirect
{


    /**
     * Weiterleitung zur Action
     * @param $controller
     */
    public static function toAction($action){
        $url = URL::urlFromAction($action);
        header('Location: ' . $url);
        exit;
    }

}
