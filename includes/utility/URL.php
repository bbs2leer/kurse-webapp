<?php

/**
 * Class URL
 */
class URL {


    /**
     * Gibt die Adresse der Webanwendung zurück
     * @return string
     */
    public static function getServerAddress(){
        return (isset($_SERVER['HTTPS']) ? "https://" : "http://") . $_SERVER["HTTP_HOST"];
    }


    /**
     * Gibt die absolute Adresse einer Aktion zurück
     * @param $action
     * @return string
     */
    public static function urlFromAction($action){
        return Configuration::$path . "/" . self::pathFromAction($action);
        //return self::getServerAddress() . "/" . self::pathFromAction($action); deprecated
    }


    /**
     * Gibt den relativen Pfad sowie Query einer Aktion zurück
     * @param $action
     * @return mixed
     */
    public static function pathFromAction($action){
        $path = "index.php?action=" . $action;
        return self::parseURL($path);

    }


    /**
     * Gibt zurück, ob mod_rewrite aktiv ist
     * @return bool
     */
    public static function modRewriteEnabled(){
        return array_key_exists('HTTP_MOD_REWRITE', $_SERVER);
    }


    /**
     * Parst die gesamte URL
     * @param $url
     * @return mixed
     */
    public static function parseURL($url){

        //Prüfung, ob mod_rewrite aktiv ist
        if (self::modRewriteEnabled()){

            //Parameter aus der URL entnehmen
            $parts = parse_url($url);
            parse_str($parts['query'], $query);

            //Prüfen, ob es sich bei der Abfrage um ein Prüfungsfach handelt
            if (!empty($query['exam'])){
                return "action/" . $query['action'] . "/exam/" . $query['exam'];
            }

            return "action/" . $query['action'];
        }

        return $url;
    }

}