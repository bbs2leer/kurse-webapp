<?php

/**
 * Class Request
 */
class Request
{


    /**
     * Filter Get-Parameter
     * @param $name
     * @param int $filter
     * @return mixed
     */
    public static function get($name, $filter = FILTER_DEFAULT){
        return self::filter($name, INPUT_GET, $filter);
    }


    /**
     * Filter Get-Parameter (INT)
     * @param $name
     * @param int $filter
     * @return mixed
     */
    public static function getInt($name, $filter = FILTER_VALIDATE_INT){
        return self::filter($name, INPUT_GET, $filter);
    }


    /**
     * Filter Post-Parameter
     * @param $name
     * @param int $filter
     * @return mixed
     */
    public static function post($name, $filter = FILTER_DEFAULT){
        return self::filter($name, INPUT_POST, $filter);
    }


    /**
     * Filter Post-Parameter (INT)
     * @param $name
     * @param int $filter
     * @return mixed
     */
    public static function postInt($name, $filter = FILTER_VALIDATE_INT){
        return self::filter($name, INPUT_POST, $filter);
    }


    /**
     * Anwendung von Filtern
     * @param $value
     * @param $type
     * @param $filter
     * @return mixed|string
     */
    private static function filter($value, $type, $filter){
        $value = filter_input($type, $value, $filter);
        $value = strip_tags($value);
        $value = htmlspecialchars($value);
        return $value;
    }

}
