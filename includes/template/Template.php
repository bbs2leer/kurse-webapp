<?php
if (!defined('APP')) {
    exit();
}

class Template
{

    /**
     * Ordner mit Templates
     * @var string
     */
    private $templateDir = "templates/";


    /**
     * Der linke Delimter für einen Standard-Platzhalter.
     * @var       string
     */
    private $leftDelimiter = '{$';


    /**
     * Der rechte Delimter für einen Standard-Platzhalter.
     * @var       string
     */
    private $rightDelimiter = '}';


    /**
     * Der linke Delimter für eine Funktion.
     * @var       string
     */
    private $leftDelimiterF = '{';


    /**
     * Der rechte Delimter für eine Funktion.
     * @var       string
     */
    private $rightDelimiterF = '}';


    /**
     * Der komplette Pfad der Templatedatei.
     * @var       string
     */
    private $templateFile = "";


    /**
     * Der Inhalt des Templates.
     * @var       string
     */
    private $template = "";


    /**
     * Eine Templatedatei öffnen.
     * @param     string $file Dateiname des Templates.
     * @return    boolean
     */
    public function load($file)
    {
        // Eigenschaften zuweisen
        $this->templateFile = $this->templateDir . $file;

        // Wenn ein Dateiname übergeben wurde, versuchen, die Datei zu öffnen
        if (!empty($this->templateFile)) {

            if (file_exists($this->templateFile)) {
                $this->template = file_get_contents($this->templateFile);
            } else {
                return false;
            }
        } else {
            return false;
        }

        // Funktionen parsen
        $this->parseFunctions();
    }


    /**
     * Einen Standard-Platzhalter ersetzen.
     * @param     string $replace Name des Platzhalters.
     * @param     string $replacement Der Text, mit dem der Platzhalter ersetzt werden soll.
     */
    public function assign($replace, $replacement)
    {
        $this->template = str_replace($this->leftDelimiter . $replace . $this->rightDelimiter, $replacement, $this->template);
    }


    /**
     * Includes parsen und Kommentare aus dem Template entfernen.
     *
     * @access    private
     */
    private function parseFunctions()
    {
        $pattern = "/" . $this->leftDelimiterF . "include file=\"(.*)\.(.*)\"" . $this->rightDelimiterF . "/";

        // Includes ersetzen ( {include file="..."} )
        while (preg_match($pattern, $this->template)) {
            $this->template = preg_replace_callback($pattern, array(&$this, 'replaceCallback'), $this->template);
        }
    }


    /**
     * Includes einbinden
     * @param $matches
     * @return bool|string
     */
    private function replaceCallback(&$matches)
    {
        $file = "./templates/" . $matches[1] . "." . $matches[2];

        if (!file_exists($file)) {
            trigger_error('File "' . $file . '" not found', E_USER_WARNING);
            return "";
        }

        return file_get_contents($file, true);
    }


    /**
     * Ausgabe des Templates
     */
    public function render()
    {
        echo $this->template;
    }

}
