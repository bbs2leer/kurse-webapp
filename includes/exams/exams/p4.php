<?php
require_once("helpers.php");

if (!defined('APP')) {
    exit();
}

/**
 * Class P4
 */
class P4
{


    /**
     * Gibt die Auswahlmöglichkeiten zurück
     * @return array
     */
    public static function getOptions()
    {
        $p2 = Storage::getCourse(2);
        $p3 = Storage::getCourse(3);

        //Fall 1, wenn P2 und P3 Deutsch und fortgeführte Fremdsprache sind
        if (($p2 == "de" && $p3 == "en") || ($p2 == "en" && $p3 == "de")) {

            ////Gibt Kurse zurück, prüft ob RE und GE erlaubt sind und fügt diese ggf. hinzu
            return PHelper::filterREGEAllowed(array("w", "iv", "ma", "ph"));
        }


        //Fall 2, wenn P2 und P3 Deutsch und Mathe oder Physik sind
        if (($p2 == "de" && ($p3 == "ma" || $p3 == "ph")) || ($p2 == "ma" && $p3 == "de") || ($p2 == "ph" && $p3 == "de")) {
            $c = array("w", "en");

            //Filtert MA und PH
            $c = PHelper::filterIfInP4P5($c, "ma");
            $c = PHelper::filterIfInP4P5($c, "ph");

            //Zweite Fremdsprache aus der 11. Klasse
            if (PHelper::hasSecondParticipatedForeignLanguage()) {
                array_push($c, Storage::getSecondParticipatedForeignLanguage());
            }

            //Wenn P2/P3 Physik ist, muss P4/P5 ein Kernfach sein
            if (($p2 != "ph") && ($p3 != "ph")) {
                array_push($c, "iv");

                //Gibt zurück, ob RE und GE zugelassen sind
                if (PHelper::isREGEAllowed()) {
                    array_push($c, "ge");
                    array_push($c, "re");
                }
            }

            return $c;
        }

        //Fall 3, wenn P2 und P3 Englisch und Mathe oder Physik sind
        if (($p2 == "en" && ($p3 == "ma" || $p3 == "ph")) || ($p2 == "ma" && $p3 == "en") || ($p2 == "ph" && $p3 == "en")) {
            $c = array("w", "de");

            //Filtert MA und PH
            $c = PHelper::filterIfInP4P5($c, "ma");
            $c = PHelper::filterIfInP4P5($c, "ph");

            //Wenn P2/P3 Physik ist, muss P4/P5 ein Kernfach sein
            if (($p2 != "ph") && ($p3 != "ph")) {
                array_push($c, "iv");

                //Gibt zurück, ob RE und GE zugelassen sind
                if (PHelper::isREGEAllowed()) {
                    array_push($c, "ge");
                    array_push($c, "re");
                }
            }

            return $c;
        }

        return null;
    }


}