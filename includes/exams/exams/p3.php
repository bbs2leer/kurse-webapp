<?php

if (!defined('APP')) {
    exit();
}

/**
 * Class P3
 */
class P3
{


    /**
     * Gibt die Auswahlmöglichkeiten zurück
     * @return array
     */
    public static function getOptions()
    {

        switch (Storage::getCourse(2)) {

            case "de":
                return array("en", "ma", "ph");
                break;

            case "en":
                return array("de", "ma", "ph");
                break;

            case "ma":
                return array("de", "en");
                break;

            case "ph":
                return array("de", "en");
                break;

            default:
                return array();

        }
    }


}