<?php

if (!defined('APP')) {
    exit();
}

/**
 * Class P2
 */
class P2
{


    /**
     * Gibt die Auswahlmöglichkeiten zurück
     * @return array
     */
    public static function getOptions()
    {
        return array("ma", "de", "en", "ph");
    }


}