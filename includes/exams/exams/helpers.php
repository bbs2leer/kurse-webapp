<?php

if (!defined('APP')) {
    exit();
}

class PHelper
{

    /**
     * Dieses Fach kann nur gewählt werden, wenn in der Einführungs- und der Qualifikationsphase
     * nicht die Pflicht zur durchgehenden Teilnahme am Unterricht in einer weiteren Fremdsprache besteht.
     * @return bool
     */
    public static function isREGEAllowed()
    {
        return !self::hasSecondParticipatedForeignLanguage();
    }


    /**
     * Filtert das Array, wenn RE und GE erlaubt sind und gibt dieses zurück
     * @param $arr
     * @return mixed
     */
    public static function filterREGEAllowed($arr){
        if (self::isREGEAllowed()){
            array_push($arr, "RE");
            array_push($arr, "GE");
        }
        return $arr;
    }


    /**
     * Gibt zurück, ob eine zweite Fremdsprache in der Einführungsphase belegt wurde
     * @return bool
     */
    public static function hasSecondParticipatedForeignLanguage()
    {
        return (Storage::getSecondParticipatedForeignLanguage() != 'none');
    }

    /**
     * Prüft, ob ein Kurse bereits in P2 oder P3 gewählt wurde. Fügt diesen hinzu, wenn es nicht der Fall ist
     * @param $arr
     * @param $course
     * @return mixed
     */
    public static function filterIfInP4P5($arr, $course){
        if (Storage::getCourse(2) != $course && Storage::getCourse(3) != $course) {
            array_push($arr, $course);
        }
        return $arr;
    }


}