<?php
require_once("exams/p2.php");
require_once("exams/p3.php");
require_once("exams/p4.php");
require_once("exams/p5.php");

if (!defined('APP')) {
    exit();
}

class Exams
{

    /**
     * @var
     */
    private $examNumber;


    /**
     * @var string
     */
    private $markup = "";


    /**
     * @var string
     */
    private $description = "";


    /**
     * Exams constructor.
     * @param $examNumber
     */
    public function __construct($examNumber)
    {
        $this->examNumber = $examNumber;
        $this->setDescription();
        if ($this->saveSubject()) {

            // Ein Fach wurde gespeichert, Weiterleitung zum nächsten
            if (!empty($this->nextExam())) {

                // Weiterleitung zum nächsten Fach
                Redirect::toAction("course&exam=" . $this->nextExam());

            } else {

                // Kurse sind gewählt, Weiterleitung zur finalen Übersicht
                Redirect::toAction("overview");
            }

        } else {

            // Kein Fach ausgewählt, somit die Auswahlmöglichkeiten zeigen
            $this->displayForm();
        }
    }


    /**
     * Gibt das gesamte Markup zurück
     * @return string
     */
    public function getMarkup()
    {
        return $this->markup;
    }


    /**
     * Gibt die Beschreibung zurück
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }


    /**
     * Gibt das nächste Prüfungsfach zurück. Gibt es kein nächstes, so wird null zurückgegeben
     * @return int|null
     */
    private function nextExam()
    {
        $next = $this->examNumber + 1;
        if ($next <= 5) {
            return $next;
        } else {
            return null;
        }
    }

    /**
     * Gibt das letzte Prüfungsfach zurück. Gibt es kein vorheriges, so wird null zurückgegeben
     * @return int|null
     */
    private function prevExam()
    {
        $prev = $this->examNumber - 1;

        // Lösche den Wert vom aktuellen Code
        Storage::deleteCourse($this->examNumber);

        if ($prev >= 1) {
            return $prev;
        } else {
            return 0;
        }
    }



    /**
     * Darstellung des Formulars
     */
    private function displayForm()
    {
        // Ausnahme für Prüfungsfach 1
        if ($this->examNumber == 1) {
            $subjects = Courses::getMainCourses();
        } else {

            //Prüfen, ob der vorherige Kurs gewählt wurde
            if (!empty($courseVal = Storage::getCourse(self::prevExam()))){

                // Auswahlmöglichkeiten ermitteln
                switch ($this->examNumber) {
                    case 2:
                        $subjects = P2::getOptions();
                        break;

                    case 3:
                        $subjects = P3::getOptions();
                        break;

                    case 4:
                        $subjects = P4::getOptions();
                        break;

                    case 5:
                        $subjects = P5::getOptions();
                        break;
                }

                // Auswahlmöglichkeiten mappen
                $subjects = Courses::getMappedCourses($subjects);

            }else{
                //Zur letzten Abfrage weiterleiten
                Redirect::toAction('course&exam='.self::prevExam());
            }
        }

        // Formular erzeugen
        $this->markup = $this->generateForm($subjects);
    }


    /**
     * Erzeugung des Formulars
     * @param $subjects
     * @return string
     */
    private function generateForm($subjects)
    {
        $markup = '<form method="post">';

        //Auswahlmöglichkeit für alle Kurse generieren
        foreach ($subjects as $key => $value) {
            $markup .= $this->subjectOption($key, $value);
        }

        $markup .= '<div class="buttons clearfix"><a href="' . URL::pathFromAction('course&exam=' . $this->prevExam()) . '" class="btn btn-primary btn-lg">Zurück</a><input class="btn btn-primary btn-lg" type="submit" value="Weiter"></div></form>';

        return $markup;
    }


    /**
     * Erzeugung der einzelnen Auswahlmöglichkeiten
     * @param $subject
     * @param $label
     * @return string
     */
    private function subjectOption($subject, $label)
    {
        return '<input id="' . $subject . '" type="radio" class="radio" name="subject" value="' . $subject . '" /><label class="option ' . $subject . '" for="' . $subject . '" title="Prüfungsfach ' . $label . '">' . $label . '</label>';
    }


    /**
     * Speichert ein Prüfungsfach und gibt den Zustand zurück
     * @return bool
     */
    private function saveSubject()
    {
        $subject = Request::post("subject");

        //Ausnahme für Prüfungsfach 1
        if ($this->examNumber == 1) {
            Storage::setMainCourse($subject);

            //Mappen von Schwerpunkt mit Kurs
            $subject = Courses::getMappedMainCourses($subject);
        }

        //Prüft, ob das Prüfungsfach existiert und speichert
        if (array_key_exists($subject, Courses::getCourses())) {
            Storage::setCourse($this->examNumber, $subject);
            return true;
        } else {
            return false;
        }
    }


    /**
     * Beschreibung für jedes Prüfungsfach festlegen
     */
    private function setDescription()
    {
        switch ($this->examNumber) {

            case 1:
                $this->description = 'Sie wählen zunächst Ihren Schwerpunkt. Anhand des Schwerpunktes wird Ihr Prüfungsfach 1 ermittelt. <span style="display: block; margin-top: 15px; font-weight: bold;">Prüfungsfach anhand des Schwerpunktes: </span><ul><li>Gestaltungs- und Medientechnik: Drucktechnik</li><li>Mechatronik: Mechatronik</li><li>Informationstechnik: Informatik</li></ul>';
                break;

            case 2:
            case 3:
                $this->description = 'Wählen Sie Ihr Prüfungsfach <strong>' . $this->examNumber . '</strong> (erhöhte Anforderungen).';
                break;
            case 4:
            case 5:
                $this->description = 'Wählen Sie Ihr Prüfungsfach <strong>' . $this->examNumber . '</strong> (grundlegende Anforderungen).';
                break;

            default:
                $this->description = 'Wählen Sie Ihr Prüfungsfach <strong>' . $this->examNumber . '</strong>.';
        }
    }

}
