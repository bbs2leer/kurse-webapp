<?php

// Laden von Konfiguration
require_once 'config/config.php';

// Laden von Funktionen zu Kursen
require_once 'courses/courses.php';

// Laden von nützlichen Funktionen
require_once 'utility/loader.php';

// Aufruf der Template-Klasse
require_once 'template/Template.php';

//Aufruf der Storage-Klasse
require_once("storage/Storage.php");

// Aufruf der Mailer-Klasse
require_once 'mailer/Mailer.php';

//Aufruf der Prüfungsklasse
require_once("exams/Exams.php");

// Aufruf des ActionHandlers
require_once("controller/ActionHandler.php");
