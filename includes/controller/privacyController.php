<?php
if (!defined('APP')) {
    exit();
}

/**
 * Controller für den Datenschutz
 */

$tmpl = new Template();
$tmpl->load("privacy.html");

// Seitenelemente vorbereiten
Page::pageElements($tmpl);

// Ausgabe des Templates
$tmpl->render();
