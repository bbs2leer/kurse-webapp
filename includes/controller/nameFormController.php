<?php
if (!defined('APP')) {
    exit();
}

/**
 * Controller für den Namen
 */

// Post-Parameter laden
$firstname = Request::post('firstname');
$lastname = Request::post('lastname');

// Prüfen der Post-Parameter
if (!empty($firstname) && !empty($lastname)) {
    Storage::setFirstname($firstname);
    Storage::setLastname($lastname);

    // Weiterleitung zum nächsten Schritt
    Redirect::toAction("language");
}

// Laden des Templates
$tmpl = new Template();
$tmpl->load("nameForm.html");
$tmpl->assign("title", "Ihr Name");

// Gespeicherte Formularwerte einsetzen
$tmpl->assign("formFirstname", Storage::getFirstname());
$tmpl->assign("formLastname", Storage::getLastname());

// Sidebar erzeugen
Page::sidebar($tmpl);
Page::pageElements($tmpl);

// Beschreibung erzeugen
Page::description($tmpl, "Der erste Schritt der Kurswahl beginnt mit Ihrem Namen. Geben Sie Ihren vollständigen Vor- und Nachnamen an.");

// Ausgabe des Templates
$tmpl->render();

