<?php
if (!defined('APP')) {
    exit();
}

/**
 * Controller für die Fremdsprache
 */

// Post-Parameter laden
$participated = Request::post('participated');

// Prüfen der Post-Parameter
if (!empty($participated)) {
    Storage::setSecondParticipatedForeignLanguage($participated);

    // Weiterleitung zum nächsten Schritt
    Redirect::toAction("course");
}

// Laden des Templates
$tmpl = new Template();
$tmpl->load("languageForm.html");
$tmpl->assign("title", "Zweite Fremdsprache");
$tmpl->assign("label", "Ihre zweite Fremdsprache in Klasse 11");

// Fremdsprachkurse laden
$foreignLanguageCourses = Courses::getForeignLanguageCourses();

// Bereits gewählte Fremdsprache entfernen
unset($foreignLanguageCourses[Storage::getParticipatedForeignLanguage()]);

// Markup der Auswahlmöglichkeiten generieren
$foreignLanguageCoursesMarkup = "";
foreach ($foreignLanguageCourses as $key => $value) {
    $foreignLanguageCoursesMarkup .= "<option value='". $key ."'>".$value."</option>";
}

// Wenn keine zweite Fremdsprache besucht wurde, muss eine gewählt werden
if (Storage::getParticipatedForeignLanguage() != "none"){
    $foreignLanguageCoursesMarkup .= "<option value='none'>keine</option>";
}

// Optionen in Formular einsetzen
$tmpl->assign("participated", $foreignLanguageCoursesMarkup);

// Sidebar erzeugen
Page::sidebar($tmpl);
Page::pageElements($tmpl);

// Beschreibung erzeugen
Page::description($tmpl, "Geben Sie Ihre zweite Fremdsprache in der Klasse 11 an.");

// Ausgabe des Templates
$tmpl->render();
