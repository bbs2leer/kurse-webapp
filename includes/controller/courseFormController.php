<?php
if (!defined('APP')) {
    exit();
}

/**
 * Controller für die Kurswahl
 */

// Prüfungsfach-Parameter in Variable speichern
$examNumber = Request::getInt("exam");

// Laden des Templates
$tmpl = new Template();
$tmpl->load("courseForm.html");

//Prüfen, ob gültiges Prüfungsfach gewählt wurde
if (!empty($examNumber)) {
    if ($examNumber > 0 && $examNumber <= 5) {

        // Seite eines Prüfungsfachs
        $exam = new Exams($examNumber);
        $tmpl->assign("content", $exam->getMarkup());
        $tmpl->assign("title", "Prüfungsfach " . $examNumber);

        // Beschreibung erzeugen
        Page::description($tmpl, $exam->getDescription());

    } else {

        // Weiterleitung zur Übersichtsseite beim ungültigen Prüfungsfach
        Redirect::toAction('course');
    }
} else {

    //Prüft, ob Name hinterlegt ist
    if (empty(Storage::getLastname())){

        //Weiterleitung zur Abfrage des Namens
        Redirect::toAction("name");
    }

    // Übersichtsseite
    $tmpl->assign("content", '<a class="btn btn-primary btn-lg" style="" href="'.URL::pathFromAction('course&exam=1').'">Prüfungsfächer wählen</a>');
    $tmpl->assign("title", "Auswahl Ihrer Prüfungsfächer");

    // Beschreibung erzeugen
    Page::description($tmpl, 'Gemäß der Verordnung über berufsbildene Schulen (BbS-VO) wählen Sie in den nächsten Schritten Ihre Prüfungsfächer und den daraus endstehenden Unterrichts- und Belegungsverpflichtungen. Beachten Sie, dass vorherige Auswahlen Ihrer Prüfungsfächer die nachfolgenden Auswahlen beeinflussen.<span style="display: block; margin-top: 15px; font-weight: bold;">Anforderungen der Prüfungsfacher:</span><ul><li>Prüfungsfacher 1 - 3: erhöhte Anforderungen</li><li>Prüfungsfacher 4 - 5: grundlegende Anforderungen</li></ul>');

}

// Sidebar erzeugen
Page::sidebar($tmpl);
Page::pageElements($tmpl);


// Ausgabe des Templates
$tmpl->render();
