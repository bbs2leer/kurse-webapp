<?php
include_once("helpers/mailContent.php");

if (!defined('APP')) {
    exit();
}

/**
 * Controller für die Übersicht
 */

// Template erstellen
$tmpl = new Template();
$tmpl->load("overview.html");

// Daten ausgeben
$tmpl->assign("title", "Ihre Prüfungsfächer");
$tmpl->assign("firstname", Storage::getFirstname());
$tmpl->assign("lastname", Storage::getLastname());
$tmpl->assign("main", Storage::getFullMainCourse());
$tmpl->assign("sub1", Storage::getFullCourse(1));
$tmpl->assign("sub2", Storage::getFullCourse(2));
$tmpl->assign("sub3", Storage::getFullCourse(3));
$tmpl->assign("sub4", Storage::getFullCourse(4));
$tmpl->assign("sub5", Storage::getFullCourse(5));

// Captcha laden
$tmpl->assign("captcha", '<img src="includes/mailer/Captcha.php" alt="Captcha" class="img-thumbnail"/>');


// Mail-Versand behandeln
$mail = new Mailer();
if (Request::postInt("mail")){
    $comment = Request::post("comment");

    // Mailer konfigurieren
    $mail->setAddress(Configuration::$mailAddress);
    $mail->setSubject(Configuration::$mailSubject);
    $mail->setMessage(generateMailContent($comment));

    // Prüft, ob Captcha korrekt
    if ($mail->checkCaptcha()){

        // Senden der Mail
        $mail->send();

        // Prüfen, ob erfolgreich gesendet
        if ($mail->getStatus()){
            Page::description($tmpl, '<strong>Erledigt!</strong> Ihre Informationen wurden erfolgreich übermittelt.', "alert-success no-print");
        }
    }else{
        Page::description($tmpl, '<strong>Fehler!</strong> Das Ergebnis war nicht korrekt. Die Informationen wurden nicht übermittelt.', "alert-danger no-print");
    }

}else{

    // Wurden Daten erfasst?
    if (Storage::getCourse(5)){
        Page::description($tmpl, '<strong>Geschafft!</strong> Hier die Übersicht alle Ihrer Prüfungsfächer.', "alert-success no-print");
    }else{

        // Weiterleitung zur Abfrage der Prüfungsfacher
        Redirect::toAction("course");
    }
}

Page::pageElements($tmpl);

// Ausgabe des Templates
$tmpl->render();
