<?php
require_once("helpers/Page.php");

if (!defined('APP')) {
    exit();
}

/**
 * Class ActionHandler
 */
class ActionHandler
{

    /**
     * Behandeln des Action-Parameters
     */
    public static function handleActions()
    {
        switch (Request::get("action")) {

            case 'name':
                include 'nameFormController.php';
                break;

            case 'language':
                include 'languageFormController.php';
                break;

            case 'secondlanguage':
                include 'secondLanguageFormController.php';
                break;

            case 'course':
                include 'courseFormController.php';
                break;

            case 'overview':
                include 'overviewController.php';
                break;

            case 'privacy':
                include 'privacyController.php';
                break;

            case 'clearStorage':
                include 'clearStorageConstroller.php';
                break;

            default:
                include 'startController.php';

        };
    }

}

//Aufruf des Handlers
ActionHandler::handleActions();

