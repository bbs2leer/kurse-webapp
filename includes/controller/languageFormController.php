<?php
if (!defined('APP')) {
    exit();
}

/**
 * Controller für die Sprachwahl
 */

// Post-Parameter laden
$participated = Request::post('participated');

// Prüfen der Post-Parameter
if (!empty($participated)) {
    Storage::setParticipatedForeignLanguage($participated);

    // Weiterleitung zum nächsten Schritt
    Redirect::toAction("secondlanguage");
}

// Laden des Templates
$tmpl = new Template();
$tmpl->load("languageForm.html");
$tmpl->assign("title", "Zweite Fremdsprache");
$tmpl->assign("label", "Ihre zweite Fremdsprache");

// Gespeicherte Formularwerte einsetzen
$foreignLanguageCoursesMarkup = "";
foreach (Courses::getForeignLanguageCourses() as $key => $value) {
    $foreignLanguageCoursesMarkup .= "<option value='". $key ."'>".$value."</option>";
}
$foreignLanguageCoursesMarkup .= "<option value='other'>Andere</option>";
$foreignLanguageCoursesMarkup .= "<option value='none'>keine</option>";

// Optionen in Formular einsetzen
$tmpl->assign("participated", $foreignLanguageCoursesMarkup);

// Sidebar erzeugen
Page::sidebar($tmpl);
Page::pageElements($tmpl);

// Beschreibung erzeugen
Page::description($tmpl, 'Haben Sie in ihrer <span title="Sekundarbereich I (beispielsweise Realschule, Gynasium)" style="font-weight: bold;">vorherigen Schullaufbahn</span> eine zweite Fremdsprache (beispielsweise neben Englisch) besucht?<span style="display: block; margin-top: 15px; font-weight: bold;">Dabei müssen folgende Kritieren erfüllt worden sein: </span><ul><li>Fünf Jahre lang durchgehend teilgenommen</li><li>Mit der Note 4 oder besser abgeschlossen</li></ul>');

// Ausgabe des Templates
$tmpl->render();
