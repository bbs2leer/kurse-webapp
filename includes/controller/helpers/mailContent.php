<?php

if (!defined('APP')) {
    exit();
}

/**
 * Generiert das Markup für die Mail und gibt dies zurück
 * @return string
 */
function generateMailContent($comment = "-"){
    $mailContent = "<style>table, th, td { border: 1px solid black; border-collapse: collapse; margin: 25px 0;} th, td {padding: 3px 5px;}</style>";

    $mailContent .= "<h3>Kurswahl - Berufliches Gymnasium Technik</h3>";
    $mailContent .= "<table>";
    $mailContent .= "<tr><td>Name</td><td>". Storage::getLastname() . "</td></tr>";
    $mailContent .= "<tr><td>Vorname</td><td>". Storage::getFirstname() . "</td></tr>";
    $mailContent .= "<tr><td>Schwerpunkt</td><td>". Storage::getFullMainCourse() . "</td></tr>";
    $mailContent .= "<tr><td>Anmerkung</td><td>". $comment . "</td></tr>";
    $mailContent .= "</table>";

    $mailContent .= "<table><tr><th>Prüfungsfach</th><th>Kurs</th></tr>";
    $mailContent .= "<tr><td>1</td><td>". Storage::getFullCourse(1) . "</td></tr>";
    $mailContent .= "<tr><td>2</td><td>". Storage::getFullCourse(2) . "</td></tr>";
    $mailContent .= "<tr><td>3</td><td>". Storage::getFullCourse(3) . "</td></tr>";
    $mailContent .= "<tr><td>4</td><td>". Storage::getFullCourse(4) . "</td></tr>";
    $mailContent .= "<tr><td>5</td><td>". Storage::getFullCourse(5) . "</td></tr>";
    $mailContent .= "</table>";

    $mailContent .= "<i>Die E-Mail wurde automatisch generiert.</i>";

    return $mailContent;
}

