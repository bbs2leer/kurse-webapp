<?php
if (!defined('APP')) {
    exit();
}

/**
 * Class Page
 * Sorgt für das korrekte Markup für Seitenelemente
 */
class Page
{


    /**
     * Liefert das Markup für die Elemente Navigation und Footer
     * @param $tmpl
     */
    public static function pageElements($tmpl)
    {
        $tmpl->assign("navigation", self::generateNavigation());
        $tmpl->assign("footer", self::generateFooter());
        $tmpl->assign("path", Configuration::$path);
    }

    /**
     * Liefert das Markup für die Beschreibung
     * @param $tmpl
     * @param $description
     * @param string $type
     */
    public static function description($tmpl, $description, $type = "alert-info")
    {

        // Ausgabe der Beschreibung
        $tmpl->assign("description", $description);
        $tmpl->assign("descriptionType", $type);
    }


    /**
     * Liefert das Markup für die Sidebar
     * @param $tmpl
     */
    public static function sidebar($tmpl)
    {

        // Ausgabe des Namens in der Sidebar
        if (!empty(Storage::getFirstname()) && !empty(Storage::getLastname())) {
            $tmpl->assign("firstname", Storage::getFirstname());
            $tmpl->assign("lastname", Storage::getLastname());
        } else {
            $tmpl->assign("firstname", "kein Name festgelegt");
            $tmpl->assign("lastname", "");
        }

        // Ausgabe der Kurse in der Sidebar
        $tmpl->assign("main", Storage::getFullMainCourse());
        $tmpl->assign("sub1", Storage::getFullCourse(1));
        $tmpl->assign("sub2", Storage::getFullCourse(2));
        $tmpl->assign("sub3", Storage::getFullCourse(3));
        $tmpl->assign("sub4", Storage::getFullCourse(4));
        $tmpl->assign("sub5", Storage::getFullCourse(5));
    }


    /**
     * Generiert Navigation
     * @return string
     */
    private static function generateNavigation()
    {

        //Menüitems
        $items = array(
            array(
                "title" => "Start",
                "href" => Configuration::$path,
                "target" => ""
            ),
            array(
                "title" => "Datenschutz",
                "href" => URL::pathFromAction("privacy"),
                "target" => ""
            ),
            array(
                "title" => "Impressum",
                "href" => "https://www.bbs2leer.de/index.php?option=com_content&view=article&id=189&Itemid=167",
                "target" => "_blank"
            )
        );

        $html = "";
        for ($i = 0; $i < count($items); ++$i) {
            $html .= self::generateMenuItem($items[$i]['title'], $items[$i]['href'], $items[$i]['target']);
        }

        return $html;
    }


    /**
     * Generiert Footer
     * @return string
     */
    private static function generateFooter()
    {

        //Menüitems
        $items = array(
            array(
                "title" => "Start",
                "href" => Configuration::$path,
                "target" => ""
            ),
            array(
                "title" => "Datenschutz",
                "href" => URL::pathFromAction("privacy"),
                "target" => ""
            ),
            array(
                "title" => "Impressum",
                "href" => "https://www.bbs2leer.de/index.php?option=com_content&view=article&id=189&Itemid=167",
                "target" => "_blank"
            )
        );

        $html = "";
        for ($i = 0; $i < count($items); ++$i) {
            $html .= self::generateMenuItem($items[$i]['title'], $items[$i]['href'], $items[$i]['target']);
        }

        return $html;
    }

    /**
     * Funktion zur Erzeugung von einzelnen Menüpunkten
     * @param $title
     * @param $href
     * @param string $target
     * @return string
     */
    private static function generateMenuItem($title, $href, $target = "")
    {
        return '<li class=""><a title="' . $title . '" href="' . $href . '" target="' . $target . '">' . $title . '</a></li>';
    }

}
