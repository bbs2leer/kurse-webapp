<?php
if (!defined('APP')) {
    exit();
}

/**
 * Controller für die Startseite
 */

$tmpl = new Template();
$tmpl->load("start.html");

// Daten ausgeben
$tmpl->assign("title", "Kurswahl");
$tmpl->assign("subtitle", "Berufliches Gymasium Technik");
$tmpl->assign("startLink", URL::pathFromAction("name") );

//Seitenelemente laden
Page::pageElements($tmpl);

// Ausgabe des Templates
$tmpl->render();
