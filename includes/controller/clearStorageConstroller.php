<?php
if (!defined('APP')) {
    exit();
}

/**
 * Controller für das Löschen der Daten
 * Session entfernen und weiterleiten
 */

Storage::destroy();
Redirect::toAction("name");
